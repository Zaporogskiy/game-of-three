package org.challenge.codding.game.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Data
public class ValidationResult {

    private int input;
    private ValidationStatus status;
    private ErrorType errorType;

    public ValidationResult(ValidationStatus status, int input) {
        this.status = status;
        this.input = input;
    }

    public ValidationResult(ValidationStatus status, int input, ErrorType errorType) {
        this.status = status;
        this.input = input;
        this.errorType = errorType;
    }

    @Getter
    @AllArgsConstructor
    @ToString
    public enum ValidationStatus {
        PASSED,
        SHOULD_BE_UPDATED,
        FAILED,
        GAME_OVER
    }

    @Getter
    @AllArgsConstructor
    @ToString
    public enum ErrorType {
        UNKNOWN_ERROR("Unknown error");

        private final String description;
    }
}
