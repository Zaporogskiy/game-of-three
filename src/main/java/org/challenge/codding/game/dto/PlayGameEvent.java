package org.challenge.codding.game.dto;

import lombok.Data;

@Data
public class PlayGameEvent {
    private final int input;
}
