package org.challenge.codding.game.controller;

import lombok.RequiredArgsConstructor;
import org.challenge.codding.game.service.GameService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/game")
@RequiredArgsConstructor
public class GameController {

    private final GameService gameOfTreeService;

    @PostMapping("/start")
    public void startGame() {
        gameOfTreeService.startGame();
    }
}
