package org.challenge.codding.game.consumer;

import lombok.extern.slf4j.Slf4j;
import org.challenge.codding.game.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GameConsumer {

    private final GameService gameOfTreeService;

    @Autowired
    public GameConsumer(GameService gameOfTreeService) {
        this.gameOfTreeService = gameOfTreeService;
    }

    @KafkaListener(
            topics = "${kafka.topic.play-game}",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(String event) {
        log.info("PlayGameEvent was consumed {} ", event);
        gameOfTreeService.play(Integer.parseInt(event));
    }
}
