package org.challenge.codding.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreeOfGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreeOfGameApplication.class, args);
	}
}
