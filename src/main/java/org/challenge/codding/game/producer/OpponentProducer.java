package org.challenge.codding.game.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.springframework.kafka.support.KafkaHeaders.MESSAGE_KEY;
import static org.springframework.kafka.support.KafkaHeaders.TOPIC;

@Slf4j
@Component
public class OpponentProducer {

    private final String opponentTopic;
    private final KafkaTemplate<String, String> opponentKafkaTemplate;

    public OpponentProducer(@Value("${kafka.topic.opponent}") String opponentTopic,
                            KafkaTemplate<String, String> opponentKafkaTemplate) {
        this.opponentTopic = opponentTopic;
        this.opponentKafkaTemplate = opponentKafkaTemplate;
    }

    public void send(int event) {
        log.info("Event {} was sent to topic {}", event, opponentTopic);
        Message<String> message = prepareMessageEvent(event);
        opponentKafkaTemplate.send(message);
        opponentKafkaTemplate.flush();
    }

    private Message<String> prepareMessageEvent(int payload) {
        return MessageBuilder
                .withPayload(String.valueOf(payload))
                .setHeader(MESSAGE_KEY, String.valueOf(payload))
                .setHeader(TOPIC, opponentTopic)
                .build();
    }
}
