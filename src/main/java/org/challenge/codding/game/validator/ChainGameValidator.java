package org.challenge.codding.game.validator;

import lombok.extern.slf4j.Slf4j;
import org.challenge.codding.game.dto.ValidationResult;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.challenge.codding.game.dto.ValidationResult.ErrorType.UNKNOWN_ERROR;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.FAILED;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.PASSED;

@Slf4j
@Service
public class ChainGameValidator implements GameValidator {

    public static final Predicate<ValidationResult> VALIDATION_FAILED = Predicate.not(result -> result.getStatus() == PASSED);
    private final List<GameValidator> validators;

    public ChainGameValidator(GameValidator multipleValidator,
                              GameValidator gameOverValidator) {
        this.validators = new ArrayList<>();
        validators.add(multipleValidator);
        validators.add(gameOverValidator);
    }

    @Override
    public ValidationResult validate(int input) {
        log.debug("Validation of the input value {} was kicked off", input);
        ValidationResult validationResult = validators.stream()
                .map(validator -> validate(input, validator))
                .filter(VALIDATION_FAILED)
                .findFirst()
                .orElseGet(() -> new ValidationResult(PASSED, input));
        log.debug("Validation of the input value {} was wrapped up. Result {}", input, validationResult);
        return validationResult;
    }

    private ValidationResult validate(int input, GameValidator validator) {
        try {
            return validator.validate(input);
        } catch (RuntimeException e) {
            return new ValidationResult(FAILED, input, UNKNOWN_ERROR);
        }
    }
}