package org.challenge.codding.game.validator;

import org.challenge.codding.game.dto.ValidationResult;

public interface GameValidator {

    ValidationResult validate(int input);
}
