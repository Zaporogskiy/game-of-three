package org.challenge.codding.game.validator;

import org.challenge.codding.game.dto.ValidationResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.GAME_OVER;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.PASSED;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.SHOULD_BE_UPDATED;

@Component
public class GameOverValidator implements GameValidator {

    @Value("${multiplicity}")
    private int multiplicity;

    @Override
    public ValidationResult validate(int input) {
        return input / multiplicity == 1
                ? new ValidationResult(GAME_OVER, input)
                : new ValidationResult(PASSED, input);
    }
}
