package org.challenge.codding.game.service;

public interface GameService {

    void startGame();

    void play(int input);
}
