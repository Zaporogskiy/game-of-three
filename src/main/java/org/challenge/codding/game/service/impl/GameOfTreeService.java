package org.challenge.codding.game.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.challenge.codding.game.dto.ValidationResult;
import org.challenge.codding.game.producer.OpponentProducer;
import org.challenge.codding.game.service.GameService;
import org.challenge.codding.game.util.RandomUtil;
import org.challenge.codding.game.validator.GameValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.FAILED;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.GAME_OVER;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.PASSED;
import static org.challenge.codding.game.dto.ValidationResult.ValidationStatus.SHOULD_BE_UPDATED;

@Slf4j
@Service
public class GameOfTreeService implements GameService {

    private final int increasingValue;
    private final String applicationName;
    private final OpponentProducer opponentProducer;
    private final GameValidator chainGameValidator;
    private final int multiplicity;

    public GameOfTreeService(@Value("${increasingValue}") int increasingValue,
                             @Value("${application.name}") String applicationName,
                             @Value("${multiplicity}")int multiplicity,
                             OpponentProducer opponentProducer,
                             GameValidator chainGameValidator) {
        this.applicationName = applicationName;
        this.increasingValue = increasingValue;
        this.opponentProducer = opponentProducer;
        this.chainGameValidator = chainGameValidator;
        this.multiplicity = multiplicity;
    }

    @Override
    public void startGame() {
        int input = RandomUtil.randomIntValue(1, Integer.MAX_VALUE);
        ValidationResult validationResult = chainGameValidator.validate(input);

        handleFailedPath(validationResult);
        handlePassedPath(validationResult);
    }

    @Override
    public void play(int input) {
        log.info("Play value {}", input);
        ValidationResult validationResult = chainGameValidator.validate(input);

        handleFailedPath(validationResult);
        handlePassedPath(validationResult);
        handleUpdatedPath(validationResult);
        handleGameOverPath(validationResult);
    }

    private void handleGameOverPath(ValidationResult validationResult) {
        Optional.of(validationResult)
                .filter(result -> GAME_OVER == result.getStatus())
                .ifPresent(result -> log.info("Game is over. Player {} is won", applicationName));
    }

    private void handlePassedPath(ValidationResult validationResult) {
        Optional.of(validationResult)
                .filter(result -> PASSED == result.getStatus())
                .map(result -> result.getInput() / multiplicity)
                .ifPresent(opponentProducer::send);
    }

    private void handleUpdatedPath(ValidationResult validationResult) {
        Optional.of(validationResult)
                .filter(result -> SHOULD_BE_UPDATED == result.getStatus())
                .map(ValidationResult::getInput)
                .map(this::increaseValue)
                .ifPresent(opponentProducer::send);
    }

    private void handleFailedPath(ValidationResult validationResult) {
        Optional.of(validationResult)
                .filter(result -> FAILED == result.getStatus())
                .ifPresent(result -> log.warn("Game can't be proceed. Reason {}", result.getErrorType().getDescription()));
    }

    private int increaseValue(int input) {
        int increased = input / multiplicity == 1
                ? input - increasingValue
                : input + increasingValue;

        log.info("Value {} was increased to {}", input, increased);
        return increased;
    }
}
