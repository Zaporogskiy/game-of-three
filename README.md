# Game of Three

##Precondition:
1. Install/Run docker. 
2. terminal -> mvn clean install
3. terminal -> docker build -t game-of-three .
4. Go to game/infrastructure folder: terminal -> docker-compose up

##Start game: 
1. Make POST request  http://localhost:8080/game/start
2. Check logs of the containers.
3. Game will be wrapped up by itself.
4. Winner will be logged.


##Requirements: 
The goal is to implement a game with two independent agents – the “players” – communicating with each other using an interface. 

When a player starts, a random whole number is generated and sent to the other player, which indicates the start of the game. The receiving player must now add one of { -1, 0, 1 } to get a number that is divisible by 3 and then divide it. The resulting number is then sent back to the original sender. 

The same rules apply until one of the players reaches the number 1 after division, which ends the game. 

Game flow:
![](game_flow.png)