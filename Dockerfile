FROM openjdk:11
MAINTAINER zaporozhskiy
ADD target/game-of-three.jar game-of-three.jar
ENTRYPOINT ["java", "-jar","game-of-three.jar"]
EXPOSE 8080